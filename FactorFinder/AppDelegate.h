//
//  AppDelegate.h
//  FactorFinder
//
//  Created by Dave Carlton on 3/24/23.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

